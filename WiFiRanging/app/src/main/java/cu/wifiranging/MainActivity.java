package cu.wifiranging;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageManager;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    // Timing
    public static long startTime = 0;
    public static long endTime = 0;

    String TAG = "MainActivity";

    public static TextView textStatus = null;
    ToggleButton buttonStartTx= null;
    ToggleButton buttonStartRx = null;

    WifiP2pManager.Channel mChannel;
    WifiP2pManager mManager;
    BroadcastReceiver receiver;

    // TXT RECORD properties
    public static final String TXTRECORD_PROP_AVAILABLE = "available";
    public static final String SERVICE_INSTANCE = "_wifidemotest";
    public static final String SERVICE_REG_TYPE = "_presence._tcp";

    final HashMap<String, String> buddies = new HashMap<String, String>();

    long SERVICE_DISCOVERING_INTERVAL = 10000;
    WifiP2pDnsSdServiceRequest serviceRequest;
    Handler mServiceDiscoveringHandler = new Handler();
    Runnable mServiceDiscoveringRunnable = new Runnable() {
        @Override
        public void run() {
            startServiceDiscovery();
        }
    };

    long SERVICE_BROADCASTING_INTERVAL = 10000; // ms
    Handler mServiceBroadcastHandler = new Handler();
    Runnable mServiceBroadcastingRunnable = new Runnable() {
        @Override
        public void run() {
            if(mManager == null)
            {
                return;
            }
            mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                }

                @Override
                public void onFailure(int error) {
                }
            });
            mServiceBroadcastHandler
                    .postDelayed(mServiceBroadcastingRunnable, SERVICE_BROADCASTING_INTERVAL);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Determine if device supports wifi direct
        if(isWifiDirectSupported(this)) {
            Log.d(TAG, "Wifi Direct supported");
        }
        else {
            Log.d(TAG, "Wifi Direct not supported");
        }

        textStatus = (TextView) findViewById(R.id.textStatus);

        // Initialize button for starting Rx/test
        buttonStartRx = (ToggleButton) findViewById(R.id.toggleButtonRx);
        buttonStartRx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked) {
                    Log.d(TAG, "Starting Rx");
                    textStatus.setText("");

                    final IntentFilter intentFilter = new IntentFilter();

                    // Indicates a change in the Wi-Fi P2P status.
                    intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);

                    // Indicates a change in the list of available peers.
                    intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);

                    // Indicates the state of Wi-Fi P2P connectivity has changed.
                    intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);

                    // Indicates this device's details have changed.
                    intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

                    mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
                    mChannel = mManager.initialize(MainActivity.this, getMainLooper(), null);

                    startRegistration();
                    discoverService();
                    textStatus.setText("Rx setup complete");

                    receiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, MainActivity.this);
                    registerReceiver(receiver, intentFilter);

                    Log.d(TAG, "Rx Receiver registered");
                    startTime = System.currentTimeMillis();
                }
                else {
                    Log.d(TAG, "Stopping Rx");
                    textStatus.setText("");
                    startTime = 0;
                    unregisterReceiver(receiver);
                    mManager.clearLocalServices(mChannel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "Local Services Successfully cleared");
                        }

                        @Override
                        public void onFailure(int error) {
                            // react to failure of clearing the local services
                            Log.d(TAG, "Local Services unsuccessfully cleared: " + String.valueOf(error));
                        }
                    });

                    mManager.stopPeerDiscovery(mChannel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "Peer discovery stopped successfully");
                        }

                        @Override
                        public void onFailure(int error) {
                            // react to failure of clearing the local services
                            Log.d(TAG, "Peer discovery stopped unsuccessfully: " + String.valueOf(error));
                        }
                    });

                    mManager = null;

                }

            }
        });

        // Toggle button for Txing
        buttonStartTx = (ToggleButton) findViewById(R.id.toggleButtonTx);
        buttonStartTx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked) {
                    Log.d(TAG, "Starting Tx");
                    textStatus.setText("");

                    final IntentFilter intentFilter = new IntentFilter();

                    // Indicates a change in the Wi-Fi P2P status.
                    intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);

                    // Indicates a change in the list of available peers.
                    intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);

                    // Indicates the state of Wi-Fi P2P connectivity has changed.
                    intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);

                    // Indicates this device's details have changed.
                    intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

                    mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
                    mChannel = mManager.initialize(MainActivity.this, getMainLooper(), null);

                    startRegistration();
                    discoverService();
                    textStatus.setText("Tx setup complete");

                    receiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, MainActivity.this);
                    registerReceiver(receiver, intentFilter);

                    Log.d(TAG, "Tx Receiver registered");
                }

                else {
                    Log.d(TAG, "Stopping Tx");
                    textStatus.setText("");
                    unregisterReceiver(receiver);
                    mManager.clearLocalServices(mChannel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "Local Services Successfully cleared");
                        }

                        @Override
                        public void onFailure(int error) {
                            // react to failure of clearing the local services
                            Log.d(TAG, "Local Services unsuccessfully cleared: " + String.valueOf(error));
                        }
                    });

                    mManager.stopPeerDiscovery(mChannel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "Peer discovery stopped successfully");
                        }

                        @Override
                        public void onFailure(int error) {
                            // react to failure of clearing the local services
                            Log.d(TAG, "Peer discovery stopped unsuccessfully: " + String.valueOf(error));
                        }
                    });

                    mManager = null;
                }
            }
        });
    }

    private void startRegistration() {

        Log.d(TAG, "startRegistration setup");

        //  Create a string map containing information about your service.
        Map record = new HashMap();
        record.put(TXTRECORD_PROP_AVAILABLE, "visible");
        //record.put("listenport", String.valueOf(1337));
        //record.put("buddyname", "John Doe" + (int) (Math.random() * 1000));
        //record.put("available", "visible");

        // Service information.  Pass it an instance name, service type
        // _protocol._transportlayer , and the map containing
        // information other devices will want once they connect to this one.
        WifiP2pDnsSdServiceInfo serviceInfo =
                WifiP2pDnsSdServiceInfo.newInstance(SERVICE_INSTANCE, SERVICE_REG_TYPE, record);

        // Add the local service, sending the service info, network channel,
        // and listener that will be used to indicate success or failure of
        // the request.
        mManager.addLocalService(mChannel, serviceInfo, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Tx Service Local Service Success");
                // Command successful! Code isn't necessarily needed here,
                // Unless you want to update the UI or add logging statements.
                mServiceBroadcastHandler.postDelayed(mServiceBroadcastingRunnable, SERVICE_BROADCASTING_INTERVAL);

            }

            @Override
            public void onFailure(int arg0) {
                // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                Log.d(TAG, "Tx Service Local Service Failure: " + String.valueOf(arg0));
            }
        });

        Log.d(TAG, "startRegistration setup complete");
    }

    private void discoverService() {

        Log.d(TAG, "discoverService setup");

        WifiP2pManager.DnsSdTxtRecordListener txtListener = new WifiP2pManager.DnsSdTxtRecordListener() {
            @Override
        /* Callback includes:
         * fullDomain: full domain name: e.g "printer._ipp._tcp.local."
         * record: TXT record dta as a map of key/value pairs.
         * device: The device running the advertised service.
         */

            public void onDnsSdTxtRecordAvailable(
                    String fullDomain, Map<String, String> record, WifiP2pDevice device) {
                Log.d(TAG, device.deviceName + " is " + record.get(TXTRECORD_PROP_AVAILABLE));
                //buddies.put(device.deviceAddress, String.valueOf(record.get("buddyname")));
            }
        };

        WifiP2pManager.DnsSdServiceResponseListener servListener = new WifiP2pManager.DnsSdServiceResponseListener() {
            @Override
            public void onDnsSdServiceAvailable(String instanceName, String registrationType,
                                                WifiP2pDevice resourceType) {

                // Update the device name with the human-friendly version from
                // the DnsTxtRecord, assuming one arrived.
                //resourceType.deviceName = buddies
                //        .containsKey(resourceType.deviceAddress) ? buddies
                //        .get(resourceType.deviceAddress) : resourceType.deviceName;

                // Add to the custom adapter defined specifically for showing
                // wifi devices.
                //WiFiDirectServicesList fragment = (WiFiDirectServicesList) getFragmentManager()
                //        .findFragmentById(R.id.frag_peerlist);
                //WiFiDevicesAdapter adapter = ((WiFiDevicesAdapter) fragment
                //        .getListAdapter());

                //adapter.add(resourceType);
                //adapter.notifyDataSetChanged();
                Log.d(TAG, "onBonjourServiceAvailable " + instanceName);

                if (instanceName.equalsIgnoreCase(SERVICE_INSTANCE)) {
                    Log.d(TAG, "Service Found: " + instanceName);
                }
            }
        };

        mManager.setDnsSdResponseListeners(mChannel, servListener, txtListener);

        serviceRequest = WifiP2pDnsSdServiceRequest.newInstance();
        /*mManager.addServiceRequest(mChannel,
                serviceRequest,
                new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        // Success!
                        Log.d(TAG, "Added service discovery request");
                    }

                    @Override
                    public void onFailure(int code) {
                        // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                        Log.d(TAG, "Failed adding service discovery request: " + String.valueOf(code));
                    }
                });

        mManager.discoverServices(mChannel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                Log.d(TAG, "Service discovery initiated");
            }

            @Override
            public void onFailure(int code) {
                // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
                Log.d(TAG, "Service discovery failed: " + String.valueOf(code));
            }
        });*/
        startServiceDiscovery();

        Log.d(TAG, "discoverService setup complete");
    }

    private void startServiceDiscovery() {
        Log.d(TAG, "startServiceDiscovery");

        if(mManager == null) {
            return;
        }
        mManager.removeServiceRequest(mChannel, serviceRequest,
                new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        //Log.d(TAG, "Service discovery request Removed");
                        mManager.addServiceRequest(mChannel, serviceRequest,
                                new WifiP2pManager.ActionListener() {

                                    @Override
                                    public void onSuccess() {

                                        //Log.d(TAG, "Added service discovery request");

                                        mManager.discoverServices(mChannel,
                                                new WifiP2pManager.ActionListener() {

                                                    @Override
                                                    public void onSuccess() {
                                                        //Log.d(TAG, "Service discovery initiated");
                                                        //service discovery started

                                                        mServiceDiscoveringHandler.postDelayed(
                                                                mServiceDiscoveringRunnable,
                                                                SERVICE_DISCOVERING_INTERVAL);
                                                    }

                                                    @Override
                                                    public void onFailure(int error) {
                                                        // react to failure of starting service discovery
                                                        //Log.d(TAG, "Service discovery failed: " + String.valueOf(error));
                                                    }
                                                });
                                    }

                                    @Override
                                    public void onFailure(int error) {
                                        // react to failure of adding service request
                                        //Log.d(TAG, "Failed adding service discovery request: " + String.valueOf(error));
                                    }
                                });
                    }

                    @Override
                    public void onFailure(int reason) {
                        // react to failure of removing service request
                        //Log.d(TAG, "Failed to remove service discovery request: " + String.valueOf(reason));

                    }
                });
    }

    // See if we support wifi direct
    private boolean isWifiDirectSupported(Context ctx) {
        PackageManager pm = ctx.getPackageManager();
        FeatureInfo[] features = pm.getSystemAvailableFeatures();
        for (FeatureInfo info : features) {
            if (info != null && info.name != null && info.name.equalsIgnoreCase("android.hardware.wifi.direct")) {
                return true;
            }
        }
        return false;
    }
}
