package cu.wifiranging;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephen on 7/14/2018.
 */

public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

    String TAG = "WifiDirectBroadCastReceiver";

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private Activity activity;


    private List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    private WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peerList) {

            Log.d(TAG, "OnPeersAvailable");

            List<WifiP2pDevice> refreshedPeers = new ArrayList<WifiP2pDevice>(peerList.getDeviceList());
            Log.d(TAG, "refreshedPeers Entries: " + String.valueOf(refreshedPeers.size()));
            if (!refreshedPeers.equals(peers)) {

                Log.d(TAG, "New Peers found");
                peers.clear();
                peers.addAll(refreshedPeers);

                // If an AdapterView is backed by this data, notify it
                // of the change. For instance, if you have a ListView of
                // available peers, trigger an update.
                //((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();

                // Perform any other updates needed based on the new list of
                // peers connected to the Wi-Fi P2P network.

                // Determine if we found the right peer
                for(int i = 0; i < peers.size(); i++) {
                    if(peers.get(i).deviceName.equals("Android_36cd") && MainActivity.startTime > 0) {
                        long time_difference = System.currentTimeMillis() - MainActivity.startTime;
                        MainActivity.textStatus.setText("Found Android_36cd: " + String.valueOf(1.0 * time_difference / 1000 + " seconds"));
                        MainActivity.startTime = 0;
                    }
                }

            }

            for(int i = 0; i < peers.size(); i++) {
                Log.d(TAG, "deviceName: " + peers.get(i).deviceName);
            }

            if (peers.size() == 0) {
                Log.d(TAG, "No devices found");
                return;
            }
        }
    };

    public WiFiDirectBroadcastReceiver(WifiP2pManager Manager, WifiP2pManager.Channel Channel, Activity activity) {
        this.mManager = Manager;
        this.mChannel = Channel;
        this.activity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {

            Log.d(TAG, "WIFI_P2P_STATE_CHANGED_ACTION");
        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {

            // The peer list has changed! We should probably do something about
            // that.
            Log.d(TAG, "WIFI_P2P_PEERS_CHANGED_ACTION");
            mManager.requestPeers(mChannel, peerListListener);

        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {

            // Connection state changed! We should probably do something about
            // that.
            Log.d(TAG, "WIFI_P2P_CONNECTION_CHANGED_ACTION");
            if (mManager == null) {
                return;
            }

            NetworkInfo networkInfo = (NetworkInfo) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            if (networkInfo.isConnected()) {

                // we are connected with the other device, request connection
                // info to find group owner IP
                Log.d(TAG, "Connected to p2p network. Requesting network details");
                mManager.requestConnectionInfo(mChannel, (WifiP2pManager.ConnectionInfoListener) activity);
            }
            else {
                // It's a disconnect
                Log.d(TAG, "WIFI_P2P_CONNECTION_CHANGED_ACTION: disconnect");
            }

        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {

            WifiP2pDevice device = (WifiP2pDevice) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
            Log.d(TAG, "Device status - " + device.status);

        }
    }
}
